import {useState, useEffect, useContext} from 'react'

import ProductCard from '../components/ProductCard';

import UserContext from '../UserContext';



export default function Product() {

	// State that will be use to store the product retrieve from the database.
	const {user, setUser} = useContext(UserContext)
	const [product, setProduct] = useState([])

	console.log(user.isAdmin)

	useEffect(() => {
		
		user.isAdmin?
		fetch(`${process.env.REACT_APP_API_URL}/products/allProduct`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			localStorage.getItem('token')

			setProduct(data.map(product =>{
				return (
					<ProductCard key={product.id} product = {product}/>
					)
			}))
		})

		:

		fetch(`${process.env.REACT_APP_API_URL}/products/activeProduct`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProduct(data.map(product =>{
				return (
					<ProductCard key={product.id} product = {product}/>
					)
			}))
		})

	}, [])

	return (
		<>

		{product}

		
		</>
		)
}