// import Banner from '../components/Banner';
// import Highlights from '../components/Highlights';

// export default function Home() {
// 	return (
// 		<>
//     		<Banner />
//     		<Highlights />

// 		</>
// 	)
// }

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

	const data = {
		title: "DataBilitz",
		content: "Set up your own computer set up!",
		destination: "/",
		label: "Sign-up now!!"
	}

	return (
		<>
			<Banner data={data} />
     		<Highlights />
     		
		</>
	)
}


