import {Link } from 'react-router-dom';
 // to link the certain tab on the Navbar

import {Button, Row, Col} from 'react-bootstrap';

export default function errorPage() {

// check if the path is not declared

	return (
		<Row>
		<Col className="p-5">
		<h1>Error 404 - Page not found</h1>
		<Link to="/">
        <Button variant="primary">Go to home Page</Button>
      </Link>
		</Col>
		</Row>
		)


}