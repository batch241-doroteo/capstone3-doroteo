
import { useState, useEffect, useContext } from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function AddProduct(){
    const {user, setUser} = useContext(UserContext);
    const [name, setName] = useState('');
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState(0);
    const [isActive, setIsActive] = useState(false);

     function product(e){
        // Prevents page from reloading
        e.preventDefault();
        console.log(user)
        fetch(`${process.env.REACT_APP_API_URL}/products/newProduct`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(res => res.json())
        .then(data => {
        console.log(data)
            data?
                     Swal.fire({
                    title: "You create a new product",
                    icon: "success",
                    text: "Product successfully added"
                })
           :
                    Swal.fire({
                    title: "Failed",
                    icon: "error",
                    text: "Please check input details!"
                    })
                
            })   
    setName('');
    setDescription('');
    setPrice('');
    }
    useEffect(() => {
        // Validation to enable submit button when all fields are populated.
        if(name !== '' && description !=='' && price !==''){
            setIsActive(true);
        } else{
            setIsActive(false);
        }
    }, [name, description, price])    

    return (
        <Form onSubmit={(e) => product(e)}>
            <Form.Group controlId="name">
                <Form.Label>Product Name</Form.Label>
                <Form.Control 
                    type="name" 
                    placeholder="Enter Product Name" 
                    value={name}
                    onChange={e => setName(e.target.value)}
                    required
                />
            </Form.Group>
            <Form.Group controlId="description">
                <Form.Label>Description</Form.Label>
                <Form.Control 
                    type="description" 
                    placeholder="Enter description of the product" 
                    value={description}
                    onChange={e => setDescription(e.target.value)}
                    required
                />
            </Form.Group>
            <Form.Group controlId="price">
                <Form.Label>Price</Form.Label>
                <Form.Control 
                    type="price" 
                    placeholder="Enter Amount" 
                    value={price}
                    onChange={e => setPrice(e.target.value)}
                    required
                />
            </Form.Group>            
             { isActive ?
                <Button variant="success" type="submit" id="submitBtn">
                Create
                </Button>
            :
                <Button variant="success" type="submit" id="submitBtn" disabled>
                Create
                </Button>
            }
        </Form>
    )
}
