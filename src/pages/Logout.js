import {useContext, useEffect} from 'react'
import { Navigate } from 'react-router-dom'

import UserContext from '../UserContext'

export default function Logout() {

	const { unsetUser, setUser} = useContext(UserContext)
	// localStorage.clear()

	// Clears the localstorage indicated the function from App.js
	unsetUser();

	useEffect(() => {
		// Sets the user state back to its original value
		setUser({id: null})
	})
	
	return (
		<Navigate to='/login'/>
		)
}