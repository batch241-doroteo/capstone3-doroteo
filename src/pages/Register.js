import { useState, useEffect, useContext } from 'react';

import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2'

import { Form, Button, Container, Row } from 'react-bootstrap';

export default function Register() {

    const {user} = useContext(UserContext);
    const navigate = useNavigate()

// State hooks to store the value of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password, setPassword] = useState('')
    const [password2, setPassword2] = useState('')

// State to determine whethere submit button will be enabled or not
    const [isActive, setIsActive] = useState(false)



// Function to simulate user registration
    function registerUser(e) {
    // Prevents page from reloading
        e.preventDefault()


        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data)

            if (data) {
                Swal.fire({
                    title: "Duplicate Email Found",
                    icon: "error",
                    text: "Please provide a different email."
                })
            } 
            else {
                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNo: mobileNo,
                        password: password
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);

                    Swal.fire({
                        title: "Registration Sucessful!",
                        icon: "success",
                        text: "Welcome to Zuitt!"
                    })
                        // Clear input fields
                    setFirstName('')
                    setLastName('')
                    setEmail('')
                    setMobileNo('')
                    setPassword('')
                    setPassword2('')
                });
            }

        })
    }


    useEffect(()=> {

    // Validation to enable submit button when ALL FIELD are populated and both password match
        if((firstName !== '' && lastName !== '' && email !== '' && mobileNo !== '' && password !== '' && password2 !== '') && (password === password2))  {
            setIsActive(true)
        } else {
            setIsActive(false)
        }


    }, [firstName, lastName, email, mobileNo, password, password2])





    return (
    //     <Form onSubmit={(e)=> registerUser(e)}>
    //     <Form.Group controlId="firstName">
    //     <Form.Label>First Name</Form.Label>
    //     <Form.Control 
    //     type="string" 
    //     placeholder="Enter First Name" 
    //     value={firstName}
    //     onChange={e=>setFirstName(e.target.value)}
    //     required
    //     />
    //     </Form.Group>
    //     <Form.Group controlId="lastName">
    //     <Form.Label>Last Name</Form.Label>
    //     <Form.Control 
    //     type="string" 
    //     placeholder="Enter Last Name" 
    //     value={lastName}
    //     onChange={e=>setLastName(e.target.value)}
    //     required
    //     />
    //     </Form.Group>
    //     <Form.Group controlId="email">
    //     <Form.Label>Email address</Form.Label>
    //     <Form.Control 
    //     type="email" 
    //     placeholder="Enter email" 
    //     required
    //     value={email}
    //     onChange={e => setEmail(e.target.value)}
    //     required
    //     />
    //     <Form.Text className="text-muted">
    //     We'll never share your email with anyone else.
    //     </Form.Text>
    //     </Form.Group>
    //     <Form.Group controlId="mobileNo">
    //     <Form.Label>Mobile Number</Form.Label>
    //     <Form.Control 
    //     type="number" 
    //     placeholder="Enter Mobile Number" 
    //     value={mobileNo}
    //     onChange={e=>setMobileNo(e.target.value)}
    //     required
    //     />
    //     </Form.Group>


    //     <Form.Group controlId="password">
    //     <Form.Label>Password</Form.Label>
    //     <Form.Control 
    //     type="password" 
    //     placeholder="Password" 
    //     value={password}
    //     onChange={e=>setPassword(e.target.value)}
    //     required
    //     />
    //     </Form.Group>

    //     <Form.Group controlId="password2">
    //     <Form.Label>Verify Password</Form.Label>
    //     <Form.Control 
    //     type="password" 
    //     placeholder="Verify Password" 
    //     value={password2}
    //     onChange={e=>setPassword2(e.target.value)}
    //     required
    //     />

    //     </Form.Group>
    //         {/*conditionally render submit button base on "isActive"*/}
    //     { isActive ?
    //     <Button variant="primary" type="submit" id="submitBtn">
    //     Submit
    //     </Button>
    //     :
    //     <Button variant="danger" type="submit" id="submitBtn">
    //     Submit
    //     </Button>
    // }

    // </Form>
<Container className="my-5">
  <Row className="justify-content-center">
        <div style={{ display: 'flex', justifyContent: 'center' }}>
        <Form style={{ width: '400px' }} onSubmit={(e) => registerUser(e)}>
        <Form.Group controlId="firstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
        type="string"
        placeholder="Enter First Name"
        value={firstName}
        onChange={(e) => setFirstName(e.target.value)}
        required
        />
        </Form.Group>
        <Form.Group controlId="lastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
        type="string"
        placeholder="Enter Last Name"
        value={lastName}
        onChange={(e) => setLastName(e.target.value)}
        required
        />
        </Form.Group>
        <Form.Group controlId="email">
        <Form.Label>Email address</Form.Label>
        <Form.Control
        type="email"
        placeholder="Enter email"
        required
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        required
        />
        <Form.Text className="text-muted">
        We'll never share your email with anyone else.
        </Form.Text>
        </Form.Group>
        <Form.Group controlId="mobileNo">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
        type="number"
        placeholder="Enter Mobile Number"
        value={mobileNo}
        onChange={(e) => setMobileNo(e.target.value)}
        required
        />
        </Form.Group>
        <Form.Group controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
        type="password"
        placeholder="Password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        required
        />
        </Form.Group>
        <Form.Group controlId="password2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control
        type="password"
        placeholder="Verify Password"
        value={password2}
        onChange={(e) => setPassword2(e.target.value)}
        required
        />
        </Form.Group>
        {/*conditionally render submit button base on "isActive"*/}
        {isActive ? (
          <Button variant="primary" type="submit" id="submitBtn">
          Submit
          </Button>
          ) : (
          <Button variant="danger" type="submit" id="submitBtn">
          Submit
          </Button>
          )}
          </Form>
          </div>
            </Row>
</Container>
          )

}