import { useState, useEffect, useContext} from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';

import {useParams, Link} from 'react-router-dom'

import UserContext from '../UserContext'

import Checkout from './Checkout'

import Form from 'react-bootstrap/Form'

import {Navigate} from 'react-router-dom'

import Swal from 'sweetalert2'

let totalAmount


export default function ProductView() {

	const {user} = useContext(UserContext)

	const {_id} = useParams()
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity,setQuantity] = useState(0)
	const [totalAmount,setTotalAmount] = useState(0)
	const [checkOuts,setCheckOuts] = useState(false)
	const [isCheckOutEnabled,setIsCheckOutEnabled] = useState(false)

	const token = localStorage.getItem('token')
	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/getProduct`).then(res => res.json()).then(data=>{
			console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)


		})
		
		console.log(token)


	}, [_id])

	function checkOut  (e) {
	 
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/createOrder`,{
			'method': 'POST',
			headers:{
				'Authorization': `Bearer ${token}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				productId: _id,
				quantity: quantity
				
			})
		}).then(res => res.json()).then(data=> {
			console.log(data)


			Swal.fire({
				title: "Ordered Successfully",
				icon: "success",
				text: "Success!"
			})

		})

	}
	function addToCart  (e) {
	 
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/addToCart`,{
			'method': 'POST',
			headers:{
				'Authorization': `Bearer ${token}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				productId: _id,
				quantity: quantity
				
			})
		})
		.then(res => res.json())
		.then(data=> {
			console.log(data)

			Swal.fire({
				title: "Added to Cart",
				icon: "success",
				text: "Success!"
			})

		})

	}

	const noNegative = (e) => {
		if(e.target.value>0)
		{
			setIsCheckOutEnabled(true)
		} else{
			setIsCheckOutEnabled(false)
		}
		if(e.target.value<0){
			e.target.value=0

			setQuantity(e.target.value)
		}
		else{


			setTotalAmount(price * e.target.value)
			totalAmount = totalAmount
			setQuantity(e.target.value)

		}
	}
	const isCheckOut = () => {
		if(!checkOuts){
			setCheckOuts(true)
		}else{
			setCheckOuts(false)
		}
	}
	
	return (

		<>
		<Container>
		<Row>
		<Col lg={{span: 6, offset:3}} >
		<Card>
		<Card.Body className="text-center">
		<Card.Title>{name}</Card.Title>
		<Card.Subtitle>Description:</Card.Subtitle>
		<Card.Text>{description}</Card.Text>
		<Card.Subtitle>Price:</Card.Subtitle>
		<Card.Text>Php {price}</Card.Text>
		<Card.Subtitle>Total:</Card.Subtitle>
		<Card.Text>PhP {totalAmount}</Card.Text>
		<Row>
		<Col xs={{offset:3, span:6}}>
		<Card.Subtitle>Quantity:</Card.Subtitle>
		<Form.Control type="number" placeholder="Quantity" className="text-center" value= {quantity} onChange= {noNegative}
		required
		/>
		</Col>
		</Row>
		<Row >
		<Col >
		{
			(user.id === null) ?
			<Button className="m-3" variant="warning" as={Link} to={`/login`} >Login</Button>
			:
			<>

			<Button className="m-3" onClick={addToCart} variant="warning" disabled ={isCheckOutEnabled ? false : true}  >Add to Cart</Button>
			<Button variant="danger" onClick={checkOut} disabled ={isCheckOutEnabled ? false : true}>Checkout</Button>
			</>
		}
		</Col>
		</Row>

		</Card.Body>
		</Card>
		</Col>
		</Row>
		</Container>
		</>
		)
}
export {totalAmount}
