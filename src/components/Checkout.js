import { useState, useEffect, useContext} from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';

import {useParams} from 'react-router-dom'

import UserContext from '../UserContext'

import {totalAmount} from './ProductView'

import Form from 'react-bootstrap/Form'

export default function Checkout() {

	const {user} = useContext(UserContext)

	const {_id} = useParams()
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity,setQuantity] = useState(0)
	const [totalAmount,setTotalAmount] = useState(0)
	


	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`).then(res => res.json()).then(data=>{
			console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setTotalAmount(totalAmount)
			


		})

	}, [_id])

	
	return (

		<Container>
			<Row>
				<Col lg={{span: 6, offset:3}} >
					<Card>
					      <Card.Body className="text-center">
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>
					        <Card.Subtitle>Total:</Card.Subtitle>
					        <Card.Text>PhP {totalAmount}</Card.Text>
					      <Row>
					     	 <Col xs={{offset:3, span:6}}>
					   	 		 <Form.Text placeholder="Quantity" className="text-center" value= {quantity}
					   	 		 		/>
					   	 	 </Col>
					   	  </Row>
					   	  <Row >
					   	  	<Col >

					     		 <Button variant="danger"  >Checkout</Button>
					     		 <Button className="m-3" variant="warning"  >Cancel</Button>
					      	</Col>
					      </Row>

					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}
