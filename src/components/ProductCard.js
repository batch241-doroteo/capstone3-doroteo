import {useState, useEffect, useContext} from 'react';
import {Button, Card, Table, Form, Modal, Row, Container, Col} from 'react-bootstrap'
import Swal from 'sweetalert2'

import {Link, useParams} from 'react-router-dom'
import UserContext from '../UserContext'

export default function ProductCard({product, index}) {

  const {name, description, price, _id, isActive, createdOn} = product;

  const [show, setShow] = useState(false);
  const {productId} = useParams();

  const [editedName, setEditedName] = useState(name);
  const [editedDescription, setEditedDescription] = useState(description);
  const [editedPrice, setEditedPrice] = useState(price);
  const [status, setEditedStatus] = useState(isActive);

  const handleClose = () => setShow(false);
  const handleShow = (id, name, description, price) => {
    setShow(true);
    setEditedName(name);
    setEditedDescription(description);
    setEditedPrice(price);
  };

  const {user} = useContext(UserContext);

  const handleNameChange = (e) => {setEditedName(e.target.value)}
  const handleDescriptionChange = (e) => {setEditedDescription(e.target.value)}
  const handlePriceChange = (e) => {setEditedPrice(e.target.value)}
  const handleActiveChange = (e) => {setEditedStatus(e.target.value)}

  const handleSaveChanges = () => {
    // handleSaveChanges was assign to the 'Edit' button
    // SAVE CHANGES TO THE BACKEND


    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        name: editedName,
        description: editedDescription,
        price: editedPrice
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Your product changed!',
        showConfirmButton: false
      })

    });

    handleClose();
  };

  const handleDelete = (productId) => {
  if (window.confirm("Are you sure you want to delete this product?")) {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        // Optionally, you can send additional data in the request body
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Your product was deleted!',
        showConfirmButton: false
      })

      // Optionally, you can also remove the deleted product from the UI
    })
    .catch(error => {
      console.error(error);

      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'Something went wrong. Please try again later.',
        showConfirmButton: false
      })
    });
  }
};






  return (

    <>
    {(user.isAdmin)?

    <Table striped bordered hover variant="dark">
    <thead>
    <tr>
    <th>#</th>
    <th>Product Name</th>
    <th>Description</th>
    <th>Price</th>
    <th>Date Added</th>
    <th>Status</th>
    <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <tr>
    <td>{index + 1}</td>
    <td>{name}</td>
    <td>{description}</td>
    <td>{price}</td>
    <td>{createdOn}</td>
    <td>{isActive ? 'Active' : 'Not active'}</td>

    {/*EDIT BUTTON FOR MODAL*/}
    <td>
    <>
    <Button variant="success" onClick={() => handleShow(_id, name, description, price)}>
    Edit
    </Button>
    <Modal show={show} onHide={handleClose}>
    <Modal.Header closeButton>
    <Modal.Title>Edit Product</Modal.Title>
    </Modal.Header>
    <Modal.Body>
    <Form>
    <Form.Group className="mb-3" controlId="productName">
    <Form.Label>Product Name</Form.Label>
    <Form.Control type="text" placeholder="Change product name" value={editedName} onChange={handleNameChange} autoFocus />
    </Form.Group>
    <Form.Group className="mb-3" controlId="description">
    <Form.Label>Description</Form.Label>
    <Form.Control type="text" placeholder="Change description" value={editedDescription} onChange={handleDescriptionChange} autoFocus />
    </Form.Group>
    <Form.Group className="mb-3" controlId="price">
    <Form.Label>Price</Form.Label>
    <Form.Control type="number" placeholder="Change amount" value={editedPrice} onChange={handlePriceChange} autoFocus />
    </Form.Group>

    <Form.Group className="mb-3" controlId="isActive">
    <Form.Label>Status</Form.Label>
    <Form.Select value={isActive} onChange={handleActiveChange}>
    <option value={true}>Active</option>
    <option value={false}>Not active</option>
    </Form.Select>
    </Form.Group>

    </Form>
    </Modal.Body>
    <Modal.Footer>
    <Button variant="secondary" onClick={handleClose}>
    Close
    </Button>
    <Button variant="primary" onClick={handleSaveChanges}>
    Save Changes
    </Button>
    </Modal.Footer>
    </Modal>
    </>

    {/*DELETE BUTTON*/}

    <button
    className="btn btn-danger" onClick={() => handleDelete(_id)}>Delete</button>
    </td>
    </tr>
    </tbody>
    </Table>

    :

    <Container>
    <Row className="justify-content-center">
    <Col className="col-11 col-md-6 col-lg-4 mb-4">
    <Card className="cardHighlight p-3">
    <Card.Body >
    <Card.Title>{name}</Card.Title>
    <Card.Subtitle>Description:</Card.Subtitle>
    <Card.Text>{description}</Card.Text>
    <Card.Subtitle>Price:</Card.Subtitle>
    <Card.Text> PHP {price}</Card.Text>
    <Button className="bg-primary" as={Link} to={`/products/${_id}`}>Add to your order </Button>

    </Card.Body>
    </Card>
    </Col>
      </Row>
    </Container>

  }

  </>




  );
}
