import {useState, useContext} from 'react';
import {Col, Form, FormControl, Button} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom'; // to link the certain tab on the Navbar
import UserContext from '../UserContext'

// import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
// import NavDropdown from 'react-bootstrap/NavDropdown';

export default function AppNavbar() {

  // State to store the user information stored in the log in page

  // const [user, setUser] = useState(localStorage.getItem('email'))

  const {user} = useContext(UserContext)


  return (
    // <Navbar bg="info" expand="lg">
    
    //     <Navbar.Brand as={Link} to='/'>DataBlitz</Navbar.Brand>
    //     <Navbar.Toggle aria-controls="basic-navbar-nav" />
    //     <Navbar.Collapse id="basic-navbar-nav">
    //       <Nav className="ml-auto">

    //           <Nav.Link as={NavLink} to="/home">Home</Nav.Link>
    //           {(user.isAdmin)?
    //           <>
    //           <Nav.Link as={NavLink} to="/product">All Products</Nav.Link>
    //           <Nav.Link as={NavLink} to="/createAdminProduct">Create Product</Nav.Link></>
    //           :
    //           <>
    //           <Nav.Link as={NavLink} to="/product">Products</Nav.Link>
    //           <Nav.Link as={NavLink} to="/cart">
    //           Cart

    //           </Nav.Link></>

    //           }              

    //           { (user.id !== null) ?
    //           <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
    //           :
    //           <>              
    
    //           <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
    //           <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
    //           </>
    //           }



    //       </Nav>
    //     </Navbar.Collapse>
    
    // </Navbar>

    <Navbar style={{ backgroundColor: "#FFB84C" }} expand="lg">
    <Navbar.Brand as={Link} to='/'>DataBilitz</Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
    <Nav className="mr-auto">
    <div className="d-flex align-items-center">
    <Form inline>
    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
    </Form>
    <Button variant="outline-light">Search</Button>
    </div>
    </Nav>
    <Nav className="ml-auto">
    <Nav.Link as={NavLink} to="/home">Home</Nav.Link>
    {(user.isAdmin) ?
    <>
    <Nav.Link as={NavLink} to="/product">Manage Products</Nav.Link>
    <Nav.Link as={NavLink} to="/createAdminProduct">Create Product</Nav.Link>
    </>
    :
    <>
    <Nav.Link as={NavLink} to="/product">Products</Nav.Link>
    <Nav.Link as={NavLink} to="/carts">Cart</Nav.Link>
    </>
  }
  { (user.id !== null) ?
  <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
  :
  <>              
  <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
  <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
  </>
}
</Nav>
</Navbar.Collapse>
</Navbar>

);
}

// export default AppNavbar; // indicate in the function
