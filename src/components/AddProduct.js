import { useState, useEffect, useContext } from 'react';

import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2'

import { Form, Button } from 'react-bootstrap';


export default function Admin() {

    const {user} = useContext(UserContext);
    const navigate = useNavigate()

// State hooks to store the value of the input fields
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [isActive, setIsActive] = useState(false)

function addProduct(e) {
    // Prevents page from reloading
        e.preventDefault()

                fetch(`${process.env.REACT_APP_API_URL}/products/newProduct`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        name: name,
                        description: description,
                        price: price,
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);

                    Swal.fire({
                        title: "Product successfully added",
                        icon: "success",
                        text: "New Product!"
                    })
                        // Clear input fields
                    setName('')
                    setDescription('')
                    setPrice('')
                    
                });
            }


useEffect(()=> {

    // Validation to enable submit button when ALL FIELD are populated and both password match
	if(name !== '' && description !== '' && price !== '')  {
		setIsActive(true)
	} else {
		setIsActive(false)
	}
}, [name, description, price])


return (

	<Form onSubmit={(e)=> addProduct(e)}>

	<Form.Group controlId="productName">
	<Form.Label>Product Name</Form.Label>
	<Form.Control 
	type="string" 
	placeholder="Create a product name" 
	value={name}
	onChange={e=>setName(e.target.value)}
	required
	/>
	</Form.Group>

	<Form.Group controlId="description">
	<Form.Label>Description</Form.Label>
	<Form.Control 
	type="string" 
	placeholder="Enter description" 
	value={description}
	onChange={e=>setDescription(e.target.value)}
	required
	/></Form.Group>

	<Form.Group controlId="price">
	<Form.Label>Price</Form.Label>
	<Form.Control 
	type="number" 
	placeholder="Enter amount" 
	value={price}
	onChange={e=>setPrice(e.target.value)}
	required
	/>
	</Form.Group>

	{ isActive ?
	<Button variant="primary" type="submit" id="submitBtn">
	Submit
	</Button>
	:
	<Button variant="danger" type="submit" id="submitBtn">
	Submit
	</Button>
}

</Form>
)

} 

