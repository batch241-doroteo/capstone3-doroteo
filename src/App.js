import './App.css';
import { useState, useEffect } from 'react'
import {UserProvider} from './UserContext'
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'

import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';
import Checkout from './components/Checkout'
// import CartCard from './components/CartCard'
// import UpdateProduct from './components/UpdateProduct'



import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import Product from './pages/Product';
import Cart from './pages/Cart';
import CreateAdminProduct from './pages/CreateAdminProduct';



import { Container } from 'react-bootstrap'

function App() {
  // This will be used to store user information that will be used for validating if a user is logged in on the app or not.
  // State hook for the user state that is defined for GLOBAL STATE.

  // const [user, setUser] = useState({email: localStorage.getItem('email')})
  const [user, setUser] = useState({
    // allows us to store and to access the properties in the 'user ID' amd 'isAdmin' data
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  // Used to check if the user information is properly stored upon login and properly cleared upon logout
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);

        // User is logged in
        if(typeof data._id !== "undefined"){
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
        })
        // User is logged out
        } else {
          setUser({
          id: null,
          isAdmin: null
          })
        }
        })
    }, [])


  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
        <AppNavbar/>
    <Container>
          <Routes>
                <Route path='/*' element={<Error/>}/>  
                
                <Route path='/home' element={<Home/>}/>
                <Route path='/register' element={<Register/>}/>
                <Route path='/login' element={<Login/>}/>
                <Route path='/logout' element={<Logout/>}/>
                
                <Route path='/product' element={<Product/>}/>
                <Route path='/createAdminProduct' element={<CreateAdminProduct/>}/>
                <Route path='/products/:_id' element={<ProductView/>}/>
                <Route path='/carts' element={<Cart/>}/>
                {/*<Route path="/products/:_id/update" element={ <UpdateProduct/>}/>*/}
            <Route path="/users/createOrder" element={ <Checkout/>}/>

                
     
          </Routes>
    </Container>
    </Router>
    </UserProvider>
    </>
    );
}

export default App;